resource "aws_iam_role_policy_attachment" "codebuild_bucket_attach" {
  count      = "${var.artifact_bucket_arn == "" ? 0 : 1}"
  role       = "${aws_iam_role.codebuild_role.name}"
  policy_arn = "${aws_iam_policy.codebuild_bucket.arn}"
}

resource "aws_iam_policy" "codebuild_bucket" {
  count       = "${var.artifact_bucket_arn == "" ? 0 : 1}"
  name        = "${var.name}-policy"
  description = "The policy to allow Codebuild to be able to write to S3 and CloudWatch Logs"
  policy      = "${data.aws_iam_policy_document.codebuild_bucket.json}"
}

data "aws_iam_policy_document" "codebuild_bucket" {
  count = "${var.artifact_bucket_arn == "" ? 0 : 1}"
  # S3 permissions
  statement {
    actions = [
      "s3:*",
    ]

    resources = [
      "${var.artifact_bucket_arn}",
      "${var.artifact_bucket_arn}/*",
    ]
  }
}