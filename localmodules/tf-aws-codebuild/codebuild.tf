resource "aws_codebuild_project" "project" {
  name           = "${var.name}"
  description    = "${var.description}"
  build_timeout  = "${var.build_timeout}"
  service_role   = "${aws_iam_role.codebuild_role.arn}"
  badge_enabled  = true

  artifacts {
    type = "${var.artifact_type}"
#    name = "${var.artifact_name}"
#    location = "${var.artifact_bucket}"
#    path = "${var.artifact_path}"
#    packaging = "${var.artifact_packaging}"
#    namespace_type = "BUILD_ID"
  }

  environment {
    compute_type    = "${var.compute_type}"
    image           = "${var.image}"
    type            = "LINUX_CONTAINER"
    privileged_mode = "true"

    environment_variable = ["${var.env_vars}"]
  }

  source {
    type      = "${var.source_type}"
    buildspec = "${var.buildspec}"
    auth {
      type = "OAUTH"
    }
    location = "${var.source_location}"
    git_clone_depth = "1"
  }
}

resource "aws_codebuild_webhook" "webhook" {
  project_name = "${aws_codebuild_project.project.name}"

  filter_group {
    filter {
      type = "EVENT"
      pattern = "PUSH"
    }

    filter {
      type = "HEAD_REF"
      pattern = "master"
    }
  }
}