variable "name" {
  type = "string"
  description = "Name for this CodeBuild"
}

variable "description" {
  type = "string"
  description = "Build Description"
  default = ""
}

variable "build_timeout" {
  type = "string"
  description = "Build Timeout"
  default = "60"
}

variable "artifact_type" {
  type = "string"
  description = "CodeBuild Artifact Type"
  default = "NO_ARTIFACTS"
}

variable "artifact_packaging" {
  type = "string"
  description = ""
  default = "NONE"
}

variable "artifact_bucket" {
  type = "string"
  description = "Bucket Name for storing build artifacts"
  default = ""
}

variable "artifact_bucket_arn" {
  type = "string"
  description = "Bucket ARN for storing build artifacts"
  default = ""
}

variable "artifact_name" {
  type = "string"
  description = "Name for the build artifact"
  default = ""
}

variable "artifact_path" {
  type = "string"
  description = "Path to the build artifact"
  default = "/"
}

variable "compute_type" {
  type = "string"
  description = "CodeBuild compute_type"
  default = "BUILD_GENERAL1_SMALL"
}

variable "image" {
  type = "string"
  description = "CodeBuild Container Image"
  default = "aws/codebuild/standard:1.0"
}

variable "env_vars" {
  description = "List of Maps of Name/Value pairs of Environment Variables to pass to the build container"
  default = []
}

variable "source_type" {
  description = "CodeBuild Source Type"
}

variable "source_location" {
  default = ""
}

variable "buildspec" {
  type = "string"
  description = "Buildspec Content"
  default = ""
}

