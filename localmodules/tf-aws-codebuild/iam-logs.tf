resource "aws_iam_role_policy_attachment" "codebuild_logs_attach" {
  role       = "${aws_iam_role.codebuild_role.name}"
  policy_arn = "${aws_iam_policy.codebuild_loga.arn}"
}

resource "aws_iam_policy" "codebuild_logs" {
  name = "${var.name}-policy"
  description = "The policy to allow Codebuild to be able to write to S3 and CloudWatch Logs"
  policy      = "${data.aws_iam_policy_document.codebuild_logs.json}"
}

data "aws_iam_policy_document" "codebuild_logs" {
  # Cloudwatch logs permissions
  statement {
    sid = "LoggingCreateLogGroup"

    actions = [
      "logs:CreateLogGroup",
    ]

    resources = [
      "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*",
    ]
  }

  statement {
    sid = "LoggingCreateLogs"

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = [
      "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:/aws/codebuild/${aws_codebuild_project.project.name}:log-stream:*",
    ]
  }
}