output "id" {
  value = "${aws_codebuild_project.project.id}"
}

output "name" {
  value = "${aws_codebuild_project.project.name}"
}

output "arn" {
  value = "${aws_codebuild_project.project.arn}"
}
output "badge_url" {
  value = "${aws_codebuild_project.project.badge_url}"
}
