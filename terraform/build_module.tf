resource "aws_s3_bucket" "build_artifacts" {
  bucket = "${var.project_name}-test"
  acl = "private"
}

module "build" {
  source = "../localmodules/tf-aws-codebuild"

  name = "${var.project_name}"
  description = "This is a test of CodeBuild"

  artifact_bucket_arn = "${aws_s3_bucket.build_artifacts.arn}"
  env_vars = [
    {
      name = "ENV_VAR"
      value = "ENV_VAR_VALUE"
    },
    {
      name = "NAME"
      value = "${var.project_name}"
    },
    {
      name = "BUCKET"
      value = "${var.project_name}-test"
    },
    {
      name = "ARTIFACT_PATH"
      value = "/${var.bucket_path}/"
    }
  ]

  source_type = "BITBUCKET"
  source_location = "https://bitbucket.org/barneyparker/test_lambda_bucket.git"
  buildspec = "${file("./buildspec.yml")}"
}
