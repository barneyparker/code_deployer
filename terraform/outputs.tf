output "webhook_payload_url" {
  value = "${aws_codebuild_webhook.webhook.payload_url}"
}

output "webhook_secret" {
  value = "${aws_codebuild_webhook.webhook.secret}"
}

output "webhook_url" {
  value = "${aws_codebuild_webhook.webhook.url}"
}

output "webhook_id" {
  value = "${aws_codebuild_webhook.webhook.id}"
}